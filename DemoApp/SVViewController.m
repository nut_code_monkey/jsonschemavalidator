//
//  SVViewController.m
//  JsonSchemaValidator
//
//  Created by Max Lunin on 23.05.13.
//  Copyright (c) 2013 Max Lunin. All rights reserved.
//

#import "SVViewController.h"
#import "SVJsonSchema.h"
#import "SVTweet.h"

@interface SVViewController () <UISearchBarDelegate>

@property (nonatomic, strong) NSArray* tweets;

@end


@interface SVModelObject : NSObject

@property (strong, nonatomic) NSString* string;
@property (strong, nonatomic) NSNumber* number;

@end


@implementation SVViewController

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tweets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TweetCell"];

    SVTweet* tweet = self.tweets[indexPath.row];
    cell.textLabel.text = tweet.fromUserName;
    cell.detailTextLabel.text = tweet.text;
    
    return cell;
}

- (void)searchTweets:( NSString* )text completion:( void(^)(NSDictionary* parsedJson, NSError* error) )completion
{
    NSParameterAssert(completion);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        id percentEscapes = [text stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://search.twitter.com/search.json?q=%@", percentEscapes]];
        
        NSURLResponse* responce = nil;
        NSError* error = nil;
        NSData* data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url]
                                             returningResponse:&responce
                                                         error:&error];
        if( error || !data )
            dispatch_async(dispatch_get_main_queue(), ^{
                completion( nil, error );
            });
        
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            completion( json, error );
        });
    });
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self searchTweets:searchBar.text completion:^(NSDictionary* json, NSError* error)
    {
        
        if ( error || !json )
        {
            NSLog(@"%@", [error localizedDescription]);
        }
        
        id results = @"results";
        
        id schema = [[SVType object] properties:@{
                     results:[[SVType array] items:[SVTweet jsonSchema]]
                     }];
        
        id validated = [schema validateJson:json error:&error];
        
        NSDictionary* instantiated = [schema instantiateValidatedJson:validated];
        
        self.tweets = instantiated[results];
        [self.tableView reloadData];
    }];
}

@end
